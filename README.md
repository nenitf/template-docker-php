<!-- 
# project

## Comandos docker
* Criar/Iniciar containers ``sudo docker-compose up ``
    * Caso o container nunca tenha sido criado, irá ser executado o Dockerfile
    * Precisa estar na pasta do arquivo ``docker-compose.yml`` para executar o comando
    * Inicia servidor em ``localhost:8056`` observando a pasta app
* Ver nomes de containes ativos `` sudo docker ps ``
* Entrar no bash/terminal do container ``sudo docker exec -it <NAME> bash``
* Encerrar todos containers ``sudo docker-compose down``
    * Precisa estar na pasta do arquivo ``docker-compose.yml`` para executar o comando
-->
# template-docker-php
Projeto/template simples para encerrar o uso do servidor embutido do php e xamp.


## Comandos docker
* Criar/Iniciar containers ``sudo docker-compose up ``
    * Caso o container nunca tenha sido criado, irá ser executado o Dockerfile
    * Precisa estar na pasta do arquivo ``docker-compose.yml`` para executar o comando
* Ver nomes de containes ativos ``sudo docker ps ``
* Entrar no bash/terminal do container ``sudo docker exec -it <NAME> bash``
* Encerrar todos containers ``sudo docker-compose down``
    * Precisa estar na pasta do arquivo ``docker-compose.yml`` para executar o comando

## Explicações
* A organização principal do projeto consiste em possuir ``docker-compose.yml`` e "Dockerfiles" em ``Dockerfiles/<nome-sugestivo-da-imagem>/Dockerfile``.
    * Sendo o primeiro responsável por especificar opções de criação de cada container (services), como portas (``local:container``) e volumes (compartilhamento de arquivos)
    * E o segundo as configurações dentro do container, como sistema operacional e downloads de bibliotecas
* A pasta app reflete no servidor.

## FAQ
* Como acessar o index.php?
> localhost:8056

## TODO
* [ ] Add ``ServerName localhost`` em /etc/apache2/apache2.conf à imagem do apache
* [ ] Instalar vim no Dockerfile do php
